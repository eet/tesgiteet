<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Perhitungan Sederhana</h1>
        
        <div>
            <form action="<c:url value="/eet"/>" method="post">
                <table>
                    <tr>
                        <td>
                            <input type="text" name="pertama" value="${pertama}"/>
                            
                        </td>
                        <td>
                            <select name="operasi">
                                <option value="+" ${operasi eq "+"? "selected='selected'":""} >+</option>
                                <option value="-" ${operasi eq "-"? "selected='selected'":""} >-</option>
                                <option value="*" ${operasi eq "*"? "selected='selected'":""} >*</option>
                                <option value="/" ${operasi eq "/"? "selected='selected'":""} >/</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" name="kedua" value="${kedua}"/>
                        </td>
                        <td>=</td>
                        <td>${hasil}</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <input style="float: right;" type="submit" value="hitung"/>
                            
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        
        
    </body>
</html>
