/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eetapp;

import com.mysql.jdbc.PreparedStatement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author eet
 */
public class EetServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cdbccoba", "root", "");

            String nama = request.getParameter("nama");
            String alamat = request.getParameter("alamat");

            PreparedStatement statement = (PreparedStatement) conn.prepareStatement("INSERT INTO karyawan (nama,alamat) VALUES (?,?)");
            statement.setString(1, nama);
            statement.setString(2, alamat);

            int rowAffected = statement.executeUpdate();


            PrintWriter out = response.getWriter();


            out.println("<html>");
            out.println("<body>");
            out.println("<h1>Row Affect " + rowAffected + "</>");
            out.println("<body>");
            out.println("<html>");

            out.flush();
            out.close();


        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EetServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map parameters = request.getParameterMap();

        String pertama = request.getParameter("pertama");
        String kedua = request.getParameter("kedua");
        String operasi = request.getParameter("operasi");

        double hasil = 0;

        if (operasi.equalsIgnoreCase("+")) {
            hasil = Double.parseDouble(pertama) + Double.parseDouble(kedua);
        } else if (operasi.equalsIgnoreCase("-")) {
            hasil = Double.parseDouble(pertama) - Double.parseDouble(kedua);
        } else if (operasi.equalsIgnoreCase("*")) {
            hasil = Double.parseDouble(pertama) * Double.parseDouble(kedua);
        } else if (operasi.equalsIgnoreCase("/")) {
            hasil = Double.parseDouble(pertama) / Double.parseDouble(kedua);
        }

        request.setAttribute("pertama", pertama);
        request.setAttribute("kedua", kedua);
        request.setAttribute("operasi", operasi);
        request.setAttribute("hasil", hasil);

        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.include(request, response);

    }
}
